package com.example.picme.widgets

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.RelativeLayout
import com.example.picme.R
import com.makeramen.roundedimageview.RoundedTransformationBuilder
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.user_avatar_layout.view.*


class UserAvatarLayout @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : RelativeLayout(context, attrs, defStyleAttr) {


    init {
        LayoutInflater.from(context).inflate(R.layout.user_avatar_layout, this, true)
    }

    fun setImageUrl(url: String) {

        val transformation = RoundedTransformationBuilder()
            .cornerRadiusDp(30f)
            .oval(true)
            .build()

        Picasso.get()
            .load(url)
            .fit()
            .placeholder(R.drawable.ic_user_default)
            .error(R.drawable.ic_user_default)
            .transform(transformation)
            .into(avatar, object : Callback {
                override fun onSuccess() {
                    loading.visibility = View.GONE
                }

                override fun onError(e: Exception?) {
                    loading.visibility = View.GONE
                }

            })
    }
}