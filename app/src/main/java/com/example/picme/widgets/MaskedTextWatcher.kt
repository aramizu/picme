package com.example.picme.widgets

import android.text.Editable
import android.widget.EditText
import android.text.TextWatcher


/**
 * Esta classe foi feita para adequar qualquer input do usuário
 * para uma máscara configurada inicialmente para um objeto deste tipo.
 * Aceita qualquer tipo de máscara, onde necessariamente '#' será trocado
 * pelo caractere atual. Ex: ##/## -> 08/16.
 */
class MaskedTextWatcher : TextWatcher {

    /*
     * Atributos da Classe
     */
    private var mMask: String = String()
    private var mMasks: ArrayList<String> = ArrayList()
    private var mEditText: EditText
    private var mIsEdited: Boolean = false
    private var mMaskSymbols: ArrayList<Char> = ArrayList()

    constructor(mEditText: EditText) {
        this.mEditText = mEditText
        mIsEdited = false
    }

    /*
     * Métodos da Classe
     */

    /**
     * Recebe a máscara no seu formato ideal.
     * Ex: ##/##; ##/##/####; #### #### #### #### ect.
     * @param mMask
     */
    fun setMask(mMask: String) {
        this.mMask = mMask
        extractMaskSymbols(mMask)
    }

    /**
     * Extrai todos os símbolos diferentes de '#' para serem utilizados
     * em tempo de digitação. Obs: ' ' (espaço) é um símbolo.
     */
    private fun extractMaskSymbols(mask: String) {
        for (i in 0 until mask.length) {
            if (mask[i] != '#' && !mMaskSymbols.contains(mask[i])) {
                mMaskSymbols.add(mask[i])
            }
        }
    }

    /**
     * Remove todos os símbolos da cadeia que chega para garantir o input 'seco'.
     * @param sequence
     * @return cadeia sem quaisquer símbolos da máscara.
     */
    private fun removeSymbols(sequence: CharSequence): String {

        var str = sequence.toString()
        for (i in 0 until mMaskSymbols.size) {
            str = str.replace(mMaskSymbols[i].toString(), "")
        }

        return str
    }


    /**
     * Remove todos os últimos caracteres se eles forem símbolos da máscara.
     * @param text
     * @return
     */
    private fun removeLastCharactersIfWasSymbol(text: String): String {
        var str = text

        var lastChar: Char? = str[str.length - 1]
        while (mMaskSymbols.contains(lastChar) && str.isNotEmpty()) {
            str = str.substring(0, str.length - 1)
            if (str.isNotEmpty()) {
                lastChar = str[str.length - 1]
            }
        }
        return str
    }

    /*
     * Eventos da Classe
     */

    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        applyMask(s, mMask)
    }

    private fun applyMask(sequence: CharSequence, maskStr: String) {
        var s = sequence
        var mask = maskStr
        if (!s.toString().isEmpty() && !mask.isEmpty()) {

            /*
             * Remove os símbolos
             */
            s = removeSymbols(s)

            /*
             * Verifica se o campo já não foi alterado. É feito isso para evitar que
             * ele passe novamente quando o campo receber o novo input (com a máscara)
             */
            if (!mIsEdited) {

                /*
                 * Substitui os caracateres de input nos '#' na sequência em que aparecem na máscara.
                 */
                for (j in 0 until s.length) {
                    if (mask.contains("#")) {
                        mask = mask.replaceFirst("#".toRegex(), s[j].toString())
                    }
                }

                /*
                 * Remove toda a parte da máscara não substituída.
                 * Ex: 0#/## -> 0; 08/## -> 08/ etc
                 */
                if (mask.contains("#")) {
                    mask = mask.substring(0, mask.indexOf('#'))
                }

                /*
                 * Remove sempre os últimos caracteres se eles forem símbolos.
                 * Isso é feito para que o backspace naturalmente retire diretamente
                 * o input do usuário e não símbolos.
                 */
                mask = removeLastCharactersIfWasSymbol(mask)
                mIsEdited = true
                mEditText.setText(mask)
            }

            /*
             * Posiciona sempre o cursor para o final.
             */
            try {
                mEditText.setSelection(mask.length)
            } catch (e: Exception) {
                mEditText.setSelection(s.length)
            }

        }
    }

    override fun afterTextChanged(s: Editable) {
        if (mIsEdited) {
            mIsEdited = false
            return
        }
    }
}
