package com.example.picme.widgets

import android.content.Context
import com.google.android.material.appbar.AppBarLayout
import androidx.appcompat.widget.Toolbar
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import android.widget.TextView
import com.example.picme.R
import kotlinx.android.synthetic.main.view_toolbar.view.*

class CustomToolbar @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {

    init {
        LayoutInflater.from(context).inflate(R.layout.view_toolbar, this, true)
    }

    fun setToolbarTitle(title: String) {
        customToolbarTextView.text = title
    }
}