package com.example.picme.extensions

import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.example.picme.R

fun FragmentManager.replaceWithoutAnimation(@IdRes id: Int, fragment: Fragment, addBackStack: Boolean) {

    val transaction = beginTransaction().replace(id, fragment)
    if (addBackStack)
        transaction.addToBackStack(null)
    transaction.commit()
}

fun FragmentManager.replaceWithSlideAnimation(@IdRes id: Int, fragment: Fragment, addBackStack: Boolean, tag: String? = null) {

    val transaction = beginTransaction()
        .setCustomAnimations(R.anim.fragment_slide_enter, R.anim.fragment_slide_exit, R.anim.fragment_slide_pop_enter, R.anim.fragment_slide_pop_exit)
        .replace(id, fragment, tag)
    if (addBackStack)
        transaction.addToBackStack(null)
    transaction.commit()
}