package com.example.picme.extensions

import com.example.picme.network.RetrofitClient
import com.example.picme.network.api.PaymentApi
import com.example.picme.network.api.UserApi

fun RetrofitClient.Companion.getUserApi(): UserApi {
    return RetrofitClient.getInstance()
            .create(UserApi::class.java)
}

fun RetrofitClient.Companion.getPaymentApi(): PaymentApi {
    return RetrofitClient.getInstance()
            .create(PaymentApi::class.java)
}