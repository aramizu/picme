package com.example.picme.extensions

import java.text.SimpleDateFormat
import java.util.*

fun Long.toDateFormat(): String {
    val date = Date(this)
    val format = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
    return format.format(date)
}

fun Long.toTimeFormat(): String {
    val date = Date(this)
    val format = SimpleDateFormat("HH:mm", Locale.getDefault())
    return format.format(date)
}