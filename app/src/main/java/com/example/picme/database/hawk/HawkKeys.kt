package com.example.picme.database.hawk

object HawkKeys {

    val CREDIT_CARD_KEY = "CREDIT_CARD_KEY"
    val STATEMENT_KEY = "STATEMENT_KEY"

}