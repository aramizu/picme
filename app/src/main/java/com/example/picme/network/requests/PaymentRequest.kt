package com.example.picme.network.requests

data class PaymentRequest(
        val card_number: String,
        var cvv: Int,
        val value: Double,
        val expiry_date: String,
        val destination_user_id: Long
)