package com.example.picme.network.responses

class ContactResponse {
    val id: Long? = null
    val name: String? = null
    val img: String? = null
    val username: String? = null
}