package com.example.picme.network.api

import com.example.picme.network.requests.PaymentRequest
import com.example.picme.network.responses.PaymentResponse
import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.POST

/**
 * Retrofit service methods
 */
interface PaymentApi {

    /**
     * Pay User
     */
    @POST("transaction")
    fun payUser(@Body paymentRequest: PaymentRequest): Single<PaymentResponse>
}
