package com.example.picme.network.responses

import com.example.picme.enums.TransactionStatus
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
class TransactionResponse {

    @JsonProperty("id")
    val id: Long? = null

    @JsonProperty("timestamp")
    val timestamp: Long? = null

    @JsonProperty("value")
    val value: Double? = null

    @JsonProperty("destination_user")
    val destinationUser: ContactResponse? = null

    @JsonProperty("success")
    val success: Boolean? = null

    @JsonProperty("status")
    val status: TransactionStatus? = null

}