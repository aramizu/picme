package com.example.picme.network.services

import com.example.picme.extensions.getUserApi
import com.example.picme.network.RetrofitClient
import com.example.picme.network.responses.ContactResponse
import io.reactivex.Single

class UserService : UserServiceContract {

    override fun getContacts(): Single<List<ContactResponse>> {
        return RetrofitClient.getUserApi().getContacts()
    }

}
