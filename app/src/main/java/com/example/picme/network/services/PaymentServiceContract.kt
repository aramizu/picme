package com.example.picme.network.services

import com.example.picme.network.requests.PaymentRequest
import com.example.picme.network.responses.PaymentResponse
import io.reactivex.Single

interface PaymentServiceContract {
    fun payUser(paymentRequest: PaymentRequest): Single<PaymentResponse>
}