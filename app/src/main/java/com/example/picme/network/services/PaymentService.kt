package com.example.picme.network.services

import com.example.picme.extensions.getPaymentApi
import com.example.picme.network.RetrofitClient
import com.example.picme.network.requests.PaymentRequest
import com.example.picme.network.responses.PaymentResponse
import io.reactivex.Completable
import io.reactivex.Single

class PaymentService : PaymentServiceContract {

    override fun payUser(paymentRequest: PaymentRequest): Single<PaymentResponse> {
        return RetrofitClient.getPaymentApi().payUser(paymentRequest)
    }
}
