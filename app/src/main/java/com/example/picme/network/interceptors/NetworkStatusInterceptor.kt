package com.example.picme.network.interceptors

import com.example.picme.config.AndroidApplication
import com.example.picme.errors.NetworkConnectionError
import com.example.picme.utils.NetworkUtils
import okhttp3.Interceptor
import okhttp3.Response

class NetworkStatusInterceptor: Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val context = AndroidApplication.applicationContext()
        val isNetworkConnected = NetworkUtils.isNetworkConnected(context)
        return if (isNetworkConnected) chain.proceed(chain.request()) else throw NetworkConnectionError()
    }
}