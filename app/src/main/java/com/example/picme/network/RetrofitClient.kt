package com.example.picme.network

import android.annotation.SuppressLint
import com.example.picme.BuildConfig
import com.example.picme.network.constants.RetrofitConstants
import com.example.picme.network.interceptors.LoggingInterceptor
import com.example.picme.network.interceptors.NetworkStatusInterceptor
import com.example.picme.network.interceptors.UserAuthInterceptor
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory
import java.text.SimpleDateFormat
import java.util.concurrent.TimeUnit

class RetrofitClient {

    companion object {
        @SuppressLint("SimpleDateFormat")
        fun getInstance(): Retrofit {

            val builder: OkHttpClient.Builder = OkHttpClient.Builder()
                    .addInterceptor(NetworkStatusInterceptor())
                    .addInterceptor(UserAuthInterceptor())
                    .addNetworkInterceptor(LoggingInterceptor().interceptor)
                    .connectTimeout(2, TimeUnit.MINUTES)
                    .writeTimeout(2, TimeUnit.MINUTES)
                    .readTimeout(2, TimeUnit.MINUTES)

            val mapper = ObjectMapper()
            mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
            mapper.dateFormat = SimpleDateFormat(RetrofitConstants.RETROFIT_DATE_FORMAT)

            return Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(JacksonConverterFactory.create(mapper))
                    .baseUrl(BuildConfig.BASE_HOST)
                    .client(builder.build())
                    .build()
        }
    }
}
