package com.example.picme.network.api

import com.example.picme.network.responses.ContactResponse
import io.reactivex.Single
import retrofit2.http.GET

/**
 * Retrofit service methods
 */
interface UserApi {

    /**
     * Contact List
     */
    @GET("users")
    fun getContacts(): Single<List<ContactResponse>>

}
