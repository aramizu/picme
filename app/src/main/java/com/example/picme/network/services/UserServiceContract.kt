package com.example.picme.network.services

import com.example.picme.network.responses.ContactResponse
import io.reactivex.Single

interface UserServiceContract {
    fun getContacts(): Single<List<ContactResponse>>
}