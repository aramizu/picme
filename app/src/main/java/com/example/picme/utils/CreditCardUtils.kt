package com.example.picme.utils

object CreditCardUtils {

    private const val MASTERCARD = "Mastercard"
    private const val MASTER = "Master"
    private const val VISA = "Visa"

    fun getCardFlag(cardNumber: String, shortFlag: Boolean = false): String {
        return if (cardNumber[0].toString() == "4") {
            VISA
        } else {
            if (shortFlag) MASTER else MASTERCARD
        }
    }
}