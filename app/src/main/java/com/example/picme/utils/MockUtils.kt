package com.example.picme.utils

import android.content.Context
import com.example.picme.R
import java.io.IOException

object MockUtils {

    fun loadJSONFromAsset(context: Context, fileName: String): String? {
        val json: String?
        try {
            val input = context.assets.open(fileName)
            val size = input.available()
            val buffer = ByteArray(size)
            input.read(buffer)
            input.close()
            json = String(buffer)
        } catch (ex: IOException) {
            ex.printStackTrace()
            return null
        }

        return json
    }

}