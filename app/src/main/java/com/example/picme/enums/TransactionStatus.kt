package com.example.picme.enums

import com.fasterxml.jackson.annotation.JsonProperty

enum class TransactionStatus(val value: String) {

    @JsonProperty("Recusada")
    DENYED("Recusada"),

    @JsonProperty("Aprovada")
    ACCEPTED("Aprovada");

    companion object {
        fun getType(value: String): TransactionStatus {
            return TransactionStatus.values().find { it.value == value }
                ?: throw IllegalArgumentException()
        }
    }
}