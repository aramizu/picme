package com.example.picme.config

import android.content.Context
import androidx.multidex.MultiDexApplication
import com.orhanobut.hawk.Hawk

class AndroidApplication: MultiDexApplication() {

    init {
        instance = this
    }

    companion object {
        private var instance: AndroidApplication? = null

        fun applicationContext() : Context {
            return instance!!.applicationContext
        }
    }

    override fun onCreate() {
        super.onCreate()
        this.initComponents()
    }

    private fun initComponents() {
        Hawk.init(this).build()
    }
}
