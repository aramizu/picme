package com.example.picme.models

import com.example.picme.enums.TransactionStatus
import com.example.picme.network.responses.ContactResponse
import com.example.picme.network.responses.TransactionResponse
import java.io.Serializable

class Transaction(transactionResponse: TransactionResponse = TransactionResponse()) : Serializable {
    val id = transactionResponse.id ?: 0
    val timestamp = transactionResponse.timestamp ?: 0L
    val value = transactionResponse.value ?: 0.0
    val destinationUser = Contact(transactionResponse.destinationUser ?: ContactResponse())
    val success = transactionResponse.success ?: false
    val status = transactionResponse.status ?: TransactionStatus.DENYED
}