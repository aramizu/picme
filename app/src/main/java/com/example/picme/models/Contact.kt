package com.example.picme.models

import com.example.picme.network.responses.ContactResponse
import java.io.Serializable

class Contact(contactResponse: ContactResponse = ContactResponse()) : Serializable {

    var userId = contactResponse.id ?: 0
    var userName = contactResponse.name ?: String()
    var profile = contactResponse.img ?: String()
    var userTagName = contactResponse.username ?: String()

}