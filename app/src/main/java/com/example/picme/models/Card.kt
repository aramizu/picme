package com.example.picme.models

data class Card(
    var cardNumber: String = String(),
    var cardOwnerName: String = String(),
    var cardExpirantion: String = String(),
    var cardSecurityCode: String = String()
)