package com.example.picme.models

import com.example.picme.network.responses.PaymentResponse
import com.example.picme.network.responses.TransactionResponse
import java.io.Serializable

class Payment(paymentResponse: PaymentResponse = PaymentResponse()) : Serializable {
    var transaction = Transaction(paymentResponse.transaction ?: TransactionResponse())
}