package com.example.picme.errors

open class BaseThrowableError: Throwable() {
    var errorMessage: String? = null
}