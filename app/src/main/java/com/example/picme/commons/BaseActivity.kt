package com.example.picme.commons

import android.content.Context
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.example.picme.R
import com.example.picme.enums.ToolbarType
import com.example.picme.widgets.CustomToolbar
import com.example.picme.widgets.LoadingDialog
import kotlinx.android.synthetic.main.view_toolbar.view.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.yesButton

abstract class BaseActivity : AppCompatActivity(), BaseContracts.View {

    private lateinit var widgetCustomToolbar: CustomToolbar
    private var loadingDialog: LoadingDialog? = null

    override fun setContentView(layoutResID: Int) {
        super.setContentView(layoutResID)
        widgetCustomToolbar = findViewById(R.id.widgetCustomToolbar)
        loadingDialog = LoadingDialog(this)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    override fun getContext(): Context {
        return this
    }

    override fun getBaseActivity(): BaseActivity? {
        return this
    }

    override fun setupToolbar(style: ToolbarType) {
        setSupportActionBar(widgetCustomToolbar.customToolbar)
        supportActionBar?.let {
            it.setDisplayShowTitleEnabled(false)

            when (style) {
                ToolbarType.NO_TOOLBAR -> widgetCustomToolbar.visibility = View.GONE
                ToolbarType.PRIMARY_TOOLBAR_NO_HOME_BUTTON -> {
                    it.setHomeButtonEnabled(false)
                    it.setDisplayHomeAsUpEnabled(false)
                    it.setHomeAsUpIndicator(null)
                }
                ToolbarType.PRIMARY_TOOLBAR_BACK_BUTTON -> {
                    it.setHomeButtonEnabled(true)
                    it.setDisplayHomeAsUpEnabled(true)
                    it.setHomeAsUpIndicator(getDrawable(R.drawable.ic_back))
                }
            }
        }
    }

    override fun setToolbarTitle(@StringRes resId: Int) {
        setToolbarTitle(getString(resId))
    }

    override fun setToolbarTitle(message: String) {
        widgetCustomToolbar.setToolbarTitle(message)
    }

    override fun showLoading() {
        loadingDialog?.showDialog()
    }

    override fun hideLoading() {
        loadingDialog?.hideDialog()
    }

    override fun showDialog(title: String?, message: String) {
        alert(message, title) {
            yesButton {
                it.dismiss()
            }
        }.show()
    }

    override fun showDialog(title: String?, message: String, functionYes: () -> Unit) {
        alert(message, title) {
            yesButton {
                functionYes()
            }
        }.show()
    }
}
