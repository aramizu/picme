package com.example.picme.commons

import android.content.Context
import androidx.annotation.StringRes
import com.example.picme.enums.ToolbarType

interface BaseContracts {

    interface View {
        fun showDialog(title: String?, message: String)
        fun showDialog(title: String?, message: String, functionYes: () -> Unit)
        fun getContext(): Context
        fun getBaseActivity(): BaseActivity?
        fun setupToolbar(style: ToolbarType)
        fun setToolbarTitle(@StringRes resId: Int)
        fun setToolbarTitle(message: String)
        fun showLoading()
        fun hideLoading()
    }
}
