package com.example.picme.modules.contact_list.presenter

import com.example.picme.R
import com.example.picme.errors.NetworkConnectionError
import com.example.picme.models.Contact
import com.example.picme.modules.contact_list.contracts.ContactListContracts
import com.example.picme.modules.contact_list.view.ContactListActivity
import com.kizitonwose.android.disposebag.DisposeBag
import com.kizitonwose.android.disposebag.disposedBy
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers

class ContactListPresenter(
    private val view: ContactListActivity,
    private val interactor: ContactListContracts.Interactor,
    private val router: ContactListContracts.Router,
    private val disposeBag: DisposeBag
) : ContactListContracts.Presenter {

    override fun onGoToPayContact(contact: Contact) {
        if (interactor.hasCard) {
            router.showPayContactScreen(contact)
        } else {
            router.showRegisterCreditCard(contact)
        }
    }

    override fun onGetContacts() {
        view.showLoading()
        interactor.getContacts()
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onSuccess = {
                    view.hideLoading()
                    view.showContacts(it)
                },
                onError = { error ->
                    view.hideLoading()

                    when (error) {
                        is NetworkConnectionError -> {
                            view.showDialog(null, view.getString(R.string.error_no_connection_message))
                        }
                        else -> {
                            view.showDialog(null, view.getString(R.string.error_unknown_message))
                        }
                    }
                }
            )
            .disposedBy(disposeBag)
    }

    override fun onGetContacts(filter: String) {
        val contacts = interactor.getContacts(filter)
        view.showContacts(contacts)
    }

    override fun showLastStatement() {
        interactor.lastStatement?.let { payment->
            interactor.lastCard?.let{ card ->
                view.showLastStatement(payment, card)
            }
        }
    }

    override fun clearStatement() {
        interactor.lastStatement = null
    }
}