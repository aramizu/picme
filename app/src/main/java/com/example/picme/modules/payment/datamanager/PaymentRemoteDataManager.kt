package com.example.picme.modules.payment.datamanager

import com.example.picme.models.Card
import com.example.picme.models.Contact
import com.example.picme.models.Payment
import com.example.picme.modules.payment.contracts.PaymentContracts
import com.example.picme.network.requests.PaymentRequest
import com.example.picme.network.responses.PaymentResponse
import com.example.picme.network.services.PaymentService
import com.example.picme.network.services.PaymentServiceContract
import io.reactivex.Completable
import io.reactivex.Single

class PaymentRemoteDataManager(
    private val paymentService: PaymentServiceContract = PaymentService()
) : PaymentContracts.Remote {

    override fun payUser(contact: Contact, card: Card, currentMoneyToSend: Double): Single<Payment> {
        val request = PaymentRequest(
            card.cardNumber,
            card.cardSecurityCode.toInt(),
            currentMoneyToSend,
            card.cardExpirantion,
            contact.userId
        )
        return paymentService.payUser(request)
            .map { Payment(it) }
    }

}