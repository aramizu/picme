package com.example.picme.modules.credit_card.submodules.register_card.contracts

import com.example.picme.models.Card

interface RegisterCardContracts {

    interface View  {
        fun showCardIfExists(card: Card?)
        var presenter: Presenter
    }

    interface Presenter {
        fun onSaveCard(card: Card)
        fun onViewCreated()
    }

    interface Interactor {
        fun saveCard(card: Card)
        fun getCard(): Card?
    }

    interface Local {
        fun saveCard(card: Card)
        fun getCard(): Card?
    }

    interface Router {
        fun goToPaymentScreen()
    }

}