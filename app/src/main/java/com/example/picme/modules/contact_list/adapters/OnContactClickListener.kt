package com.example.picme.modules.contact_list.adapters

import com.example.picme.models.Contact

interface OnContactClickListener {
    fun onContactClick(contact: Contact)
}