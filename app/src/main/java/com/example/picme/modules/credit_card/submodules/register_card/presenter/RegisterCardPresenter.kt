package com.example.picme.modules.credit_card.submodules.register_card.presenter

import com.example.picme.models.Card
import com.example.picme.models.Contact
import com.example.picme.modules.contact_list.contracts.ContactListContracts
import com.example.picme.modules.contact_list.view.ContactListActivity
import com.example.picme.modules.credit_card.submodules.register_card.contracts.RegisterCardContracts
import com.example.picme.modules.credit_card.submodules.register_card.view.RegisterCardFragment

class RegisterCardPresenter(private val view: RegisterCardFragment,
                            private val interactor: RegisterCardContracts.Interactor,
                            private val router: RegisterCardContracts.Router) : RegisterCardContracts.Presenter {

    override fun onViewCreated() {
        val card = interactor.getCard()
        view.showCardIfExists(card)
    }

    override fun onSaveCard(card: Card) {
        interactor.saveCard(card)
        router.goToPaymentScreen()
    }

}