package com.example.picme.modules.credit_card.submodules.register_card.datamanager

import com.example.picme.database.hawk.HawkKeys
import com.example.picme.models.Card
import com.example.picme.modules.credit_card.contracts.CreditCardContracts
import com.example.picme.modules.credit_card.submodules.register_card.contracts.RegisterCardContracts
import com.orhanobut.hawk.Hawk

class RegisterCardLocalDataManager : RegisterCardContracts.Local {

    override fun getCard(): Card? {
        return Hawk.get(HawkKeys.CREDIT_CARD_KEY)
    }

    override fun saveCard(card: Card) {
        Hawk.put(HawkKeys.CREDIT_CARD_KEY, card)
    }

}