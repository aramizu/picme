package com.example.picme.modules.credit_card.submodules.new_card.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.picme.R
import com.example.picme.modules.credit_card.submodules.new_card.contracts.NewCardContracts
import com.example.picme.modules.credit_card.submodules.new_card.router.NewCardRouter
import kotlinx.android.synthetic.main.fragment_new_credit_card_welcome.*

class NewCardWelcomeFragment : Fragment(), NewCardContracts.View {

    override lateinit var presenter: NewCardContracts.Presenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_new_credit_card_welcome, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        NewCardRouter.assembleModule(this)

        setupEvents()
    }

    private fun setupEvents() {
        buttonRegisterCard.setOnClickListener {
            presenter.onRegisterCardTapped()
        }
    }

}


