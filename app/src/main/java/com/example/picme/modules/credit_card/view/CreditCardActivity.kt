package com.example.picme.modules.credit_card.view

import android.os.Bundle
import com.example.picme.R
import com.example.picme.commons.BaseActivity
import com.example.picme.enums.ToolbarType
import com.example.picme.modules.credit_card.contracts.CreditCardContracts
import com.example.picme.modules.credit_card.router.CreditCardRouter

class CreditCardActivity : BaseActivity(), CreditCardContracts.View {

    override lateinit var presenter: CreditCardContracts.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_credit_card)

        CreditCardRouter.assembleModule(this)

        setupToolbar(ToolbarType.PRIMARY_TOOLBAR_BACK_BUTTON)

        presenter.onStartScreen()
    }

    fun onContinueToPaymentScreen() {
        presenter.onContinueToPaymentScreen()
    }

}
