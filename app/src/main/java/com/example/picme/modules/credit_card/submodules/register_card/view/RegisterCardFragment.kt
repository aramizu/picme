package com.example.picme.modules.credit_card.submodules.register_card.view

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.picme.R
import com.example.picme.models.Card
import com.example.picme.modules.credit_card.router.CreditCardRouter
import com.example.picme.modules.credit_card.submodules.register_card.contracts.RegisterCardContracts
import com.example.picme.modules.credit_card.submodules.register_card.router.RegisterCardRouter
import com.example.picme.widgets.MaskedTextWatcher
import kotlinx.android.synthetic.main.fragment_register_credit_card.*
import java.text.SimpleDateFormat
import java.util.*
import com.github.dewinjm.monthyearpicker.MonthYearPickerDialog
import com.github.dewinjm.monthyearpicker.MonthYearPickerDialogFragment
import org.jetbrains.anko.sdk27.coroutines.onClick


class RegisterCardFragment : Fragment(), RegisterCardContracts.View, MonthYearPickerDialog.OnDateSetListener {

    override lateinit var presenter: RegisterCardContracts.Presenter

    private lateinit var dialog: MonthYearPickerDialogFragment

    private val card = Card()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_register_credit_card, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        RegisterCardRouter.assembleModule(this)

        setupComponents()
        setupEvents()
        presenter.onViewCreated()
    }

    private fun setupComponents() {
        val cal = Calendar.getInstance()

        dialog = MonthYearPickerDialogFragment.getInstance(
            cal.get(Calendar.MONTH),
            cal.get(Calendar.YEAR),
            getString(R.string.register_credit_card_date_picker_title)
        )
        dialog.isCancelable = false
        dialog.setOnDateSetListener(this)

        val cardNumberMask = MaskedTextWatcher(editTextCardNumber)
        cardNumberMask.setMask("#### #### #### ####")
        editTextCardNumber.addTextChangedListener(cardNumberMask)
    }

    private fun setupEvents() {
        inputLayoutCardExpirationContainer.onClick {
            dialog.show(fragmentManager, null)
        }
        editTextCardNumber.addTextChangedListener(getValidationTextWatcher())
        editTextCardName.addTextChangedListener(getValidationTextWatcher())
        editTextCardCvv.addTextChangedListener(getValidationTextWatcher())
        buttonSaveCreditCard.onClick {
            presenter.onSaveCard(card)
        }
    }

    override fun showCardIfExists(card: Card?) {
        card?.let {
            editTextCardNumber.setText(it.cardNumber)
            editTextCardName.setText(it.cardOwnerName)
            editTextCardExpiration.setText(it.cardExpirantion)
            editTextCardCvv.setText(it.cardSecurityCode)
        }
    }

    override fun onDateSet(year: Int, month: Int) {
        val calendar = Calendar.getInstance()
        calendar.set(year, month, 0)
        val dateFormat = SimpleDateFormat("MM/yy", Locale.getDefault())
        editTextCardExpiration.setText(dateFormat.format(calendar.time))

        validateFields()
    }

    private fun validateFields() {
        if (editTextCardNumber.length() < 19 ||
            editTextCardName.text?.isEmpty() == true ||
            editTextCardExpiration.text?.isEmpty() == true ||
            editTextCardCvv.length() < 3) {
            buttonSaveCreditCard.visibility = View.GONE
            return
        }

        setupCard()

        buttonSaveCreditCard.visibility = View.VISIBLE
    }

    private fun setupCard() {
        card.cardNumber = editTextCardNumber.text.toString().replace(" ", "", false)
        card.cardOwnerName = editTextCardName.text.toString()
        card.cardExpirantion = editTextCardExpiration.text.toString()
        card.cardSecurityCode = editTextCardCvv.text.toString()
    }

    private fun getValidationTextWatcher(): TextWatcher {
        return object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                validateFields()
            }
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) = Unit
        }
    }

}

