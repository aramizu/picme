package com.example.picme.modules.payment.view

import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.core.content.ContextCompat
import com.example.picme.R
import com.example.picme.commons.BaseActivity
import com.example.picme.enums.ToolbarType
import com.example.picme.models.Card
import com.example.picme.models.Contact
import com.example.picme.modules.payment.contracts.PaymentContracts
import com.example.picme.modules.payment.router.PaymentRouter
import com.example.picme.utils.CreditCardUtils
import com.example.picme.utils.CurrencyUtils
import kotlinx.android.synthetic.main.activity_payment.*
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.textColor

class PaymentActivity : BaseActivity(), PaymentContracts.View {

    override lateinit var presenter: PaymentContracts.Presenter

    private var currentMoneyToSend: Double = 0.0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment)

        PaymentRouter.assembleModule(this)

        setupToolbar(ToolbarType.PRIMARY_TOOLBAR_BACK_BUTTON)
        setupStyle()
        setupListeners()

        presenter.onStartScreen()
    }

    private fun setupStyle() {
        currencySymbol.typeface = Typeface.createFromAsset(assets, "Roboto-Thin.ttf")
        inputMoney.typeface = Typeface.createFromAsset(assets, "Roboto-Thin.ttf")
    }

    private fun setupListeners() {
        inputMoney.setSelection(inputMoney.text.toString().length)
        inputMoney.requestFocus()

        inputMoney.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                inputMoney.removeTextChangedListener(this)
                inputMoney.setText(CurrencyUtils.format(s.toString()))
                inputMoney.setSelection(inputMoney.text.toString().length)
                inputMoney.addTextChangedListener(this)

                currentMoneyToSend = CurrencyUtils.getDouble(inputMoney.text.toString())
                setComponentsEnable(currentMoneyToSend > 0)
            }
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })

        buttonPay.onClick {
            presenter.onButtonPayTapped(currentMoneyToSend)
        }

        editCard.onClick {
            presenter.onButtonEditCardTapped()
        }
    }

    private fun setComponentsEnable(isEnable: Boolean) {
        buttonPay.background = ContextCompat.getDrawable(
            this,
            if (isEnable) R.drawable.rounded_primary_button_shape else R.drawable.rounded_secondary_button_shape
        )
        buttonPay.isEnabled = isEnable
        inputMoney.textColor = ContextCompat.getColor(this, if (isEnable) R.color.colorAccent else R.color.colorSecondaryText)
        currencySymbol.textColor = ContextCompat.getColor(this, if (isEnable) R.color.colorAccent else R.color.colorSecondaryText)
    }

    override fun showInformation(contact: Contact, card: Card) {
        contactTagName.text = contact.userTagName
        contactAvatar.setImageUrl(contact.profile)

        val flag = CreditCardUtils.getCardFlag(card.cardNumber)
        val finalCard = card.cardNumber.substring(card.cardNumber.length - 4, card.cardNumber.length)
        cardType.text = "$flag $finalCard • "
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 100) {
            presenter.onStartScreen()
        }
    }

}
