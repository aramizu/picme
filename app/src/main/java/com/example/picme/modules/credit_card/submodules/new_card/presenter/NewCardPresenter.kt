package com.example.picme.modules.credit_card.submodules.new_card.presenter

import com.example.picme.modules.credit_card.submodules.new_card.contracts.NewCardContracts
import com.example.picme.modules.credit_card.submodules.new_card.view.NewCardWelcomeFragment

class NewCardPresenter(
    private val router: NewCardContracts.Router
) : NewCardContracts.Presenter {

    override fun onRegisterCardTapped() {
        router.goToRegisterCard()
    }

}