package com.example.picme.modules.credit_card.interactor

import com.example.picme.modules.credit_card.contracts.CreditCardContracts

class CreditCardInteractor(private val local: CreditCardContracts.Local) : CreditCardContracts.Interactor {

    override val hasCard: Boolean
        get() {
            return local.getLastCardUsed() != null
        }

}