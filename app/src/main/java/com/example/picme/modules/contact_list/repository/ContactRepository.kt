package com.example.picme.modules.contact_list.repository

import com.example.picme.models.Card
import com.example.picme.models.Contact
import com.example.picme.models.Payment
import com.example.picme.modules.contact_list.contracts.ContactListContracts
import io.reactivex.Single

class ContactRepository(
    private val local: ContactListContracts.Local,
    private val remote: ContactListContracts.Remote
) : ContactListContracts.Repository {

    private var contacts = ArrayList<Contact>()

    override fun getContacts(filter: String): ArrayList<Contact> {
        val parameter: String = filter

        return if (parameter.isEmpty()) {
            contacts
        } else {
            contacts.filter { contact ->
                contact.userName.contains(parameter) ||
                        contact.userTagName.contains(parameter)
            } as ArrayList<Contact>
        }
    }

    override fun getContacts(): Single<ArrayList<Contact>> {
        return remote.getContacts()
            .doOnSuccess {
                contacts = it
            }
    }

    override fun getCard(): Card? {
        return local.getCard()
    }

    override fun getLastStatement(): Payment? {
        return local.getLastStatement()
    }

    override fun clearStatement() {
        local.clearStatement()
    }
}