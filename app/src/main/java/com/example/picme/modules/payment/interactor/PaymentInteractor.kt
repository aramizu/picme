package com.example.picme.modules.payment.interactor

import com.example.picme.models.Card
import com.example.picme.models.Contact
import com.example.picme.models.Payment
import com.example.picme.modules.payment.contracts.PaymentContracts
import com.example.picme.modules.payment.repository.PaymentRepository
import io.reactivex.Single

class PaymentInteractor(
    private val repository: PaymentContracts.Repository = PaymentRepository()
) : PaymentContracts.Interactor {

    override val card: Card
        get() {
            return repository.getLastCardUsed()
        }

    override fun payUser(contact: Contact, currentMoneyToSend: Double): Single<Payment> {
        return repository.payUser(contact, card, currentMoneyToSend)
    }

}