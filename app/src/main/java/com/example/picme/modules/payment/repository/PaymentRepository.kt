package com.example.picme.modules.payment.repository

import com.example.picme.models.Card
import com.example.picme.models.Contact
import com.example.picme.models.Payment
import com.example.picme.modules.payment.contracts.PaymentContracts
import com.example.picme.modules.payment.datamanager.PaymentLocalDataManager
import com.example.picme.modules.payment.datamanager.PaymentRemoteDataManager
import io.reactivex.Single

class PaymentRepository(
    private val local: PaymentContracts.Local = PaymentLocalDataManager(),
    private val remote: PaymentContracts.Remote = PaymentRemoteDataManager()
) : PaymentContracts.Repository {

    override fun getLastCardUsed(): Card {
        return local.getLastCardUsed()
    }

    override fun payUser(contact: Contact, card: Card, currentMoneyToSend: Double): Single<Payment> {
        return remote.payUser(contact, card, currentMoneyToSend)
            .doOnSuccess {
                if (it.transaction.success) {
                    local.saveLastPayment(it)
                }
            }
    }

}