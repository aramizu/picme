package com.example.picme.modules.contact_list.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.picme.R
import com.example.picme.models.Contact
import com.example.picme.utils.MockUtils
import kotlinx.android.synthetic.main.item_contact_list.view.*

class ContactsListAdapter(
        var context: Context,
        private val listener: OnContactClickListener
) : RecyclerView.Adapter<ContactsListAdapter.ContactViewHolder>() {

    private var contacts = ArrayList<Contact>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactViewHolder {
        val contactItem = LayoutInflater.from(context).inflate(R.layout.item_contact_list, parent, false)
        return ContactViewHolder(contactItem, listener)
    }

    override fun onBindViewHolder(viewHolder: ContactViewHolder, position: Int) {
        val contact = contacts[position]
        viewHolder.setupInformation(contact)
    }

    override fun getItemCount(): Int {
        return contacts.size
    }

    fun addContacts(contacts: MutableList<Contact>) {
        this.contacts.clear()
        this.contacts.addAll(contacts)
        notifyDataSetChanged()
    }

    inner class ContactViewHolder(itemView: View, private val listener: OnContactClickListener) : RecyclerView.ViewHolder(itemView) {

        private lateinit var contact: Contact

        init {
            setupListeners()
        }

        fun setupInformation(contact: Contact) {
            this.contact = contact
            itemView.contactImageProfile.setImageUrl(contact.profile)
            itemView.contactTagName.text = contact.userTagName
            itemView.contactName.text = contact.userName
        }

        private fun setupListeners() {
            itemView.contactViewContainer.setOnClickListener {
                listener.onContactClick(contact)
            }
        }
    }
}
