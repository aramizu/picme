package com.example.picme.modules.payment.router

import com.example.picme.models.Contact
import com.example.picme.modules.credit_card.view.CreditCardActivity
import com.example.picme.modules.payment.contracts.PaymentContracts
import com.example.picme.modules.payment.datamanager.PaymentLocalDataManager
import com.example.picme.modules.payment.datamanager.PaymentRemoteDataManager
import com.example.picme.modules.payment.interactor.PaymentInteractor
import com.example.picme.modules.payment.presenter.PaymentPresenter
import com.example.picme.modules.payment.repository.PaymentRepository
import com.example.picme.modules.payment.view.PaymentActivity
import com.kizitonwose.android.disposebag.DisposeBag
import org.jetbrains.anko.startActivityForResult

class PaymentRouter(private val activity: PaymentActivity) : PaymentContracts.Router {

    override fun goToRegisterCard() {
        activity.startActivityForResult<CreditCardActivity>(100)
    }

    override fun returnToContactList() {
        activity.finish()
    }

    companion object {

        const val CONTACT_EXTRA = "CONTACT_EXTRA"

        private lateinit var contact: Contact

        fun assembleModule(view: PaymentActivity) {
            contact = view.intent.extras?.getSerializable(CONTACT_EXTRA) as Contact

            val local = PaymentLocalDataManager()
            val remote = PaymentRemoteDataManager()
            val repository = PaymentRepository(local, remote)
            val interactor = PaymentInteractor(repository)
            val router = PaymentRouter(view)
            val presenter = PaymentPresenter(view, interactor, contact, router, DisposeBag(view))
            view.presenter = presenter
        }
    }
}