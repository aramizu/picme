package com.example.picme.modules.credit_card.submodules.new_card.router

import com.example.picme.R
import com.example.picme.extensions.replaceWithoutAnimation
import com.example.picme.modules.credit_card.submodules.new_card.contracts.NewCardContracts
import com.example.picme.modules.credit_card.submodules.new_card.presenter.NewCardPresenter
import com.example.picme.modules.credit_card.submodules.new_card.view.NewCardWelcomeFragment
import com.example.picme.modules.credit_card.submodules.register_card.view.RegisterCardFragment

class NewCardRouter(private val fragment: NewCardWelcomeFragment) : NewCardContracts.Router {

    override fun goToRegisterCard() {
        fragment.activity?.supportFragmentManager?.replaceWithoutAnimation(
            R.id.fragmentContainer,
            RegisterCardFragment(),
            false
        )
    }

    companion object {

        fun assembleModule(view: NewCardWelcomeFragment) {
            val router = NewCardRouter(view)
            val presenter = NewCardPresenter(router)
            view.presenter = presenter
        }
    }
}