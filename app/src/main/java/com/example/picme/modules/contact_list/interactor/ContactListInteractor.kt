package com.example.picme.modules.contact_list.interactor

import com.example.picme.models.Card
import com.example.picme.models.Contact
import com.example.picme.models.Payment
import com.example.picme.modules.contact_list.contracts.ContactListContracts
import io.reactivex.Single

class ContactListInteractor(private val repository: ContactListContracts.Repository) : ContactListContracts.Interactor {

    override val hasCard: Boolean
    get() {
        return repository.getCard() != null
    }

    override val lastCard: Card?
    get() {
        return repository.getCard()
    }

    override var lastStatement: Payment?
    get() {
        return repository.getLastStatement()
    }
    set(value) {
        if (value == null) {
            repository.clearStatement()
        }
    }

    override fun getContacts(filter: String): ArrayList<Contact> {
        return repository.getContacts(filter)
    }

    override fun getContacts(): Single<ArrayList<Contact>> {
        return repository.getContacts()
    }
}