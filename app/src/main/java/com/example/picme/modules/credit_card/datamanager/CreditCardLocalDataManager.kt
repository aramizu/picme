package com.example.picme.modules.credit_card.datamanager

import com.example.picme.database.hawk.HawkKeys
import com.example.picme.models.Card
import com.example.picme.modules.credit_card.contracts.CreditCardContracts
import com.orhanobut.hawk.Hawk

class CreditCardLocalDataManager : CreditCardContracts.Local {

    override fun getLastCardUsed(): Card? {
        return Hawk.get(HawkKeys.CREDIT_CARD_KEY)
    }

}