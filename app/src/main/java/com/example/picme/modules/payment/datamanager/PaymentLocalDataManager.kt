package com.example.picme.modules.payment.datamanager

import com.example.picme.database.hawk.HawkKeys
import com.example.picme.models.Card
import com.example.picme.models.Payment
import com.example.picme.modules.payment.contracts.PaymentContracts
import com.orhanobut.hawk.Hawk

class PaymentLocalDataManager : PaymentContracts.Local {

    override fun saveLastPayment(payment: Payment) {
        Hawk.put(HawkKeys.STATEMENT_KEY, payment)
    }

    override fun getLastCardUsed(): Card {
        return Hawk.get(HawkKeys.CREDIT_CARD_KEY)
    }

}