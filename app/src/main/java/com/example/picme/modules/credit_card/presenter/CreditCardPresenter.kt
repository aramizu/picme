package com.example.picme.modules.credit_card.presenter

import com.example.picme.modules.credit_card.contracts.CreditCardContracts
import com.example.picme.modules.credit_card.view.CreditCardActivity

class CreditCardPresenter(private val view: CreditCardActivity,
                          private val interactor: CreditCardContracts.Interactor,
                          private val router: CreditCardContracts.Router) : CreditCardContracts.Presenter {

    override fun onContinueToPaymentScreen() {
        router.showPaymentScreen()
    }

    override fun onStartScreen() {

        if (interactor.hasCard) {
            router.showCardForm()
        } else {
            router.showNewCardWelcome()
        }
    }

}