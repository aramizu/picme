package com.example.picme.modules.contact_list.datamanager

import com.example.picme.database.hawk.HawkKeys
import com.example.picme.models.Card
import com.example.picme.models.Payment
import com.example.picme.modules.contact_list.contracts.ContactListContracts
import com.orhanobut.hawk.Hawk

class ContactsListLocalDataManager : ContactListContracts.Local {

    override fun getCard(): Card? {
        return Hawk.get(HawkKeys.CREDIT_CARD_KEY)
    }

    override fun getLastStatement(): Payment? {
        return Hawk.get(HawkKeys.STATEMENT_KEY)
    }

    override fun clearStatement() {
        Hawk.delete(HawkKeys.STATEMENT_KEY)
    }
}