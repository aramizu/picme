package com.example.picme.modules.contact_list.datamanager

import com.example.picme.models.Contact
import com.example.picme.modules.contact_list.contracts.ContactListContracts
import com.example.picme.network.services.UserService
import com.example.picme.network.services.UserServiceContract
import io.reactivex.Single

class ContactsListRemoteDataManager(
    private val userService: UserServiceContract = UserService()
) : ContactListContracts.Remote {

    override fun getContacts(): Single<ArrayList<Contact>> {
        return userService.getContacts()
            .map { contacts -> contacts.map { Contact(it) } as ArrayList<Contact> }
    }

}