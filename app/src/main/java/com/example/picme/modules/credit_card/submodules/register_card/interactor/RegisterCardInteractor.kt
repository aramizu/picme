package com.example.picme.modules.credit_card.submodules.register_card.interactor

import com.example.picme.models.Card
import com.example.picme.modules.credit_card.submodules.register_card.contracts.RegisterCardContracts

class RegisterCardInteractor(private val local: RegisterCardContracts.Local) : RegisterCardContracts.Interactor {

    override fun getCard(): Card? {
        return local.getCard()
    }

    override fun saveCard(card: Card) {
        local.saveCard(card)
    }

}