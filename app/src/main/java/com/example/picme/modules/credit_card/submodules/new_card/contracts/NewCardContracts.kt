package com.example.picme.modules.credit_card.submodules.new_card.contracts

interface NewCardContracts {

    interface View  {
        var presenter: Presenter
    }

    interface Presenter {
        fun onRegisterCardTapped()
    }

    interface Router {
        fun goToRegisterCard()
    }

}