package com.example.picme.modules.credit_card.router

import com.example.picme.R
import com.example.picme.extensions.replaceWithoutAnimation
import com.example.picme.models.Contact
import com.example.picme.modules.credit_card.contracts.CreditCardContracts
import com.example.picme.modules.credit_card.datamanager.CreditCardLocalDataManager
import com.example.picme.modules.credit_card.interactor.CreditCardInteractor
import com.example.picme.modules.credit_card.presenter.CreditCardPresenter
import com.example.picme.modules.credit_card.submodules.new_card.view.NewCardWelcomeFragment
import com.example.picme.modules.credit_card.submodules.register_card.view.RegisterCardFragment
import com.example.picme.modules.credit_card.view.CreditCardActivity
import com.example.picme.modules.payment.view.PaymentActivity
import org.jetbrains.anko.startActivity

class CreditCardRouter(
    private val activity: CreditCardActivity
) : CreditCardContracts.Router {

    override fun showCardForm() {
        activity.supportFragmentManager?.replaceWithoutAnimation(
            R.id.fragmentContainer,
            RegisterCardFragment(),
            false
        )
    }

    override fun showNewCardWelcome() {
        activity.supportFragmentManager?.replaceWithoutAnimation(
            R.id.fragmentContainer,
            NewCardWelcomeFragment(),
            false
        )
    }

    override fun showPaymentScreen() {
        contact?.let {
            activity.startActivity<PaymentActivity>(CreditCardRouter.CONTACT_EXTRA to contact)
            activity.finish()
            return
        }
        activity.setResult(100)
        activity.finish()
    }

    companion object {

        const val CONTACT_EXTRA = "CONTACT_EXTRA"

        private var contact: Contact? = null

        fun assembleModule(view: CreditCardActivity) {
            contact = view.intent.extras?.getSerializable(CONTACT_EXTRA) as? Contact

            val local = CreditCardLocalDataManager()
            val interactor = CreditCardInteractor(local)
            val router = CreditCardRouter(view)
            val presenter = CreditCardPresenter(view, interactor, router)
            view.presenter = presenter
        }
    }
}