package com.example.picme.modules.payment.presenter

import com.example.picme.R
import com.example.picme.config.AndroidApplication
import com.example.picme.errors.NetworkConnectionError
import com.example.picme.errors.TransactionError
import com.example.picme.models.Contact
import com.example.picme.modules.payment.contracts.PaymentContracts
import com.example.picme.modules.payment.view.PaymentActivity
import com.kizitonwose.android.disposebag.DisposeBag
import com.kizitonwose.android.disposebag.disposedBy
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers

class PaymentPresenter(
    private val view: PaymentActivity,
    private val interactor: PaymentContracts.Interactor,
    private val contact: Contact,
    private val router: PaymentContracts.Router,
    private val disposeBag: DisposeBag
) : PaymentContracts.Presenter {


    override fun onStartScreen() {
        val card = interactor.card

        view.showInformation(contact, card)
    }

    override fun onButtonPayTapped(currentMoneyToSend: Double) {
        view.showLoading()
        interactor.payUser(contact, currentMoneyToSend)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onSuccess = {
                    view.hideLoading()
                    if (!it.transaction.success) {
                        view.showDialog(null, view.getString(R.string.error_trasaction_message)) {
                            router.returnToContactList()
                        }
                    } else {
                        router.returnToContactList()
                    }
                },
                onError = { error ->
                    view.hideLoading()

                    when (error) {
                        is NetworkConnectionError -> {
                            view.showDialog(null, view.getString(R.string.error_no_connection_message))
                        }
                    }
                }
            )
            .disposedBy(disposeBag)
    }

    override fun onButtonEditCardTapped() {
        router.goToRegisterCard()
    }

}