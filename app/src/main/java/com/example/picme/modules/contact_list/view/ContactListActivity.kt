package com.example.picme.modules.contact_list.view

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.Gravity
import android.view.View
import android.widget.EditText
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.picme.R
import com.example.picme.commons.BaseActivity
import com.example.picme.enums.ToolbarType
import com.example.picme.extensions.hideKeyboard
import com.example.picme.extensions.setupClearButtonWithAction
import com.example.picme.extensions.toDateFormat
import com.example.picme.extensions.toTimeFormat
import com.example.picme.models.Card
import com.example.picme.models.Contact
import com.example.picme.models.Payment
import com.example.picme.modules.contact_list.adapters.ContactsListAdapter
import com.example.picme.modules.contact_list.adapters.OnContactClickListener
import com.example.picme.modules.contact_list.contracts.ContactListContracts
import com.example.picme.modules.contact_list.router.ContactListRouter
import com.example.picme.utils.CreditCardUtils
import com.example.picme.utils.CurrencyUtils
import com.trafi.anchorbottomsheetbehavior.AnchorBottomSheetBehavior
import com.trafi.anchorbottomsheetbehavior.AnchorBottomSheetBehavior.*
import kotlinx.android.synthetic.main.activity_contact_list.*
import kotlinx.android.synthetic.main.layout_statement.*


class ContactListActivity : BaseActivity(), ContactListContracts.View, OnContactClickListener {

    override lateinit var presenter: ContactListContracts.Presenter

    private lateinit var contactsListAdapter: ContactsListAdapter

    private lateinit var behavior: AnchorBottomSheetBehavior<View>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact_list)

        ContactListRouter.assembleModule(this)

        setupToolbar(ToolbarType.PRIMARY_TOOLBAR_NO_HOME_BUTTON)
        setupList()
        setupListeners()
        setupBottomSheet()

        presenter.onGetContacts()
    }

    private fun setupBottomSheet() {
        behavior = AnchorBottomSheetBehavior.from(stetementContainer)
        behavior.addBottomSheetCallback(object : AnchorBottomSheetBehavior.BottomSheetCallback() {
            override fun onSlide(view: View, offset: Float) {}
            override fun onStateChanged(view: View, oldState: Int, newState: Int) {
                when (newState) {
                    STATE_COLLAPSED -> {
                        presenter.clearStatement()
                    }
                }
            }
        })
    }

    private fun setupListeners() {
        contactListSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                presenter.onGetContacts(s.toString())
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) = Unit
        })

        contactListSearch.setupClearButtonWithAction(R.drawable.ic_magnify_white)
        contactListSearch.onFocusChangeListener = View.OnFocusChangeListener { field, hasFocus ->
            if (hasFocus) {
                field.background = ContextCompat.getDrawable(this, R.drawable.rounded_input_text_focus_shape)
                (field as? EditText)?.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_magnify_white, 0, 0, 0)
            } else {
                field.background = ContextCompat.getDrawable(this, R.drawable.rounded_input_text_shape)
                (field as? EditText)?.gravity = Gravity.CENTER and Gravity.CENTER_VERTICAL
                (field as? EditText)?.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_magnify, 0, 0, 0)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        presenter.showLastStatement()
        hideKeyboard()
    }

    override fun onDestroy() {
        presenter.clearStatement()
        super.onDestroy()
    }

    private fun setupList() {
        contactsListAdapter = ContactsListAdapter(this, this)

        contactList.run {
            adapter = contactsListAdapter
            itemAnimator = DefaultItemAnimator()
            layoutManager = LinearLayoutManager(context)
        }
    }

    override fun showContacts(contacts: ArrayList<Contact>) {
        contactsListAdapter.addContacts(contacts)
    }

    override fun onContactClick(contact: Contact) {
        presenter.onGoToPayContact(contact)
    }

    override fun showLastStatement(payment: Payment, card: Card) {
        behavior.state = AnchorBottomSheetBehavior.STATE_EXPANDED

        contactAvatar.setImageUrl(payment.transaction.destinationUser.profile)
        contactTagName.text = payment.transaction.destinationUser.userTagName

        transactionDate.text = String.format(getString(R.string.statement_date_time_format),
            payment.transaction.timestamp.toDateFormat(),
            payment.transaction.timestamp.toTimeFormat()
        )

        transactionId.text = String.format(getString(R.string.statement_transaction_format), payment.transaction.id.toString())

        val flag = CreditCardUtils.getCardFlag(card.cardNumber, shortFlag = true)
        val finalCard = card.cardNumber.substring(card.cardNumber.length - 4, card.cardNumber.length)
        val cardFormat = getString(R.string.statement_card_format)
        cardDescription.text = String.format(cardFormat, flag, finalCard)

        cardTotal.text = CurrencyUtils.format(payment.transaction.value)
        totalValue.text = CurrencyUtils.format(payment.transaction.value)
    }
}
