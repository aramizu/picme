package com.example.picme.modules.payment.contracts

import com.example.picme.commons.BaseContracts
import com.example.picme.models.Card
import com.example.picme.models.Contact
import com.example.picme.models.Payment
import com.example.picme.network.responses.PaymentResponse
import io.reactivex.Completable
import io.reactivex.Single

interface PaymentContracts {

    interface View : BaseContracts.View {
        var presenter: Presenter
        fun showInformation(contact: Contact, card: Card)
    }

    interface Presenter {
        fun onStartScreen()
        fun onButtonPayTapped(currentMoneyToSend: Double)
        fun onButtonEditCardTapped()
    }

    interface Interactor {
        val card: Card
        fun payUser(contact: Contact, currentMoneyToSend: Double): Single<Payment>
    }

    interface Local {
        fun getLastCardUsed(): Card
        fun saveLastPayment(payment: Payment)
    }

    interface Remote {
        fun payUser(contact: Contact, card: Card, currentMoneyToSend: Double): Single<Payment>
    }

    interface Repository {
        fun getLastCardUsed(): Card
        fun payUser(contact: Contact, card: Card, currentMoneyToSend: Double): Single<Payment>
    }

    interface Router {
        fun returnToContactList()
        fun goToRegisterCard()
    }

}