package com.example.picme.modules.contact_list.contracts

import com.example.picme.commons.BaseContracts
import com.example.picme.models.Card
import com.example.picme.models.Contact
import com.example.picme.models.Payment
import com.example.picme.network.responses.ContactResponse
import io.reactivex.Single

interface ContactListContracts {

    interface View : BaseContracts.View {
        var presenter: Presenter
        fun showContacts(contacts: ArrayList<Contact>)
        fun showLastStatement(payment: Payment, card: Card)
    }

    interface Presenter {
        fun onGetContacts()
        fun onGetContacts(filter: String)
        fun onGoToPayContact(contact: Contact)
        fun showLastStatement()
        fun clearStatement()
    }

    interface Interactor {
        val hasCard: Boolean
        val lastCard: Card?
        var lastStatement: Payment?
        fun getContacts(): Single<ArrayList<Contact>>
        fun getContacts(filter: String): ArrayList<Contact>
    }

    interface Local {
        fun getCard(): Card?
        fun getLastStatement(): Payment?
        fun clearStatement()
    }

    interface Remote {
        fun getContacts(): Single<ArrayList<Contact>>
    }

    interface Repository {
        fun getContacts(): Single<ArrayList<Contact>>
        fun getContacts(filter: String): ArrayList<Contact>
        fun getCard(): Card?
        fun getLastStatement(): Payment?
        fun clearStatement()
    }

    interface Router {
        fun showPayContactScreen(contact: Contact)
        fun showRegisterCreditCard(contact: Contact)
    }

}