package com.example.picme.modules.credit_card.submodules.register_card.router

import com.example.picme.modules.credit_card.submodules.register_card.contracts.RegisterCardContracts
import com.example.picme.modules.credit_card.submodules.register_card.datamanager.RegisterCardLocalDataManager
import com.example.picme.modules.credit_card.submodules.register_card.interactor.RegisterCardInteractor
import com.example.picme.modules.credit_card.submodules.register_card.presenter.RegisterCardPresenter
import com.example.picme.modules.credit_card.submodules.register_card.view.RegisterCardFragment
import com.example.picme.modules.credit_card.view.CreditCardActivity

class RegisterCardRouter(private val fragment: RegisterCardFragment) : RegisterCardContracts.Router {

    override fun goToPaymentScreen() {
        (fragment.activity as? CreditCardActivity)?.onContinueToPaymentScreen()
    }

    companion object {

        fun assembleModule(view: RegisterCardFragment) {
            val local = RegisterCardLocalDataManager()
            val interactor = RegisterCardInteractor(local)
            val router = RegisterCardRouter(view)
            val presenter = RegisterCardPresenter(view, interactor, router)
            view.presenter = presenter
        }
    }
}