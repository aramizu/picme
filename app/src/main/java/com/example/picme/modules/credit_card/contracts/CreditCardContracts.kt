package com.example.picme.modules.credit_card.contracts

import com.example.picme.commons.BaseContracts
import com.example.picme.models.Card

interface CreditCardContracts {

    interface View : BaseContracts.View {
        var presenter: Presenter
    }

    interface Presenter {
        fun onStartScreen()
        fun onContinueToPaymentScreen()
    }

    interface Interactor {
        val hasCard: Boolean
    }

    interface Local {
        fun getLastCardUsed(): Card?
    }

    interface Router {
        fun showCardForm()
        fun showNewCardWelcome()
        fun showPaymentScreen()
    }

}