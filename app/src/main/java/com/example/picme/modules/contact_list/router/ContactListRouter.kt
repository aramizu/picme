package com.example.picme.modules.contact_list.router

import com.example.picme.models.Contact
import com.example.picme.modules.contact_list.contracts.ContactListContracts
import com.example.picme.modules.contact_list.datamanager.ContactsListLocalDataManager
import com.example.picme.modules.contact_list.datamanager.ContactsListRemoteDataManager
import com.example.picme.modules.contact_list.interactor.ContactListInteractor
import com.example.picme.modules.contact_list.presenter.ContactListPresenter
import com.example.picme.modules.contact_list.repository.ContactRepository
import com.example.picme.modules.contact_list.view.ContactListActivity
import com.example.picme.modules.credit_card.router.CreditCardRouter
import com.example.picme.modules.credit_card.view.CreditCardActivity
import com.example.picme.modules.payment.router.PaymentRouter
import com.example.picme.modules.payment.view.PaymentActivity
import com.kizitonwose.android.disposebag.DisposeBag
import org.jetbrains.anko.startActivity

class ContactListRouter(private val activity: ContactListActivity) : ContactListContracts.Router {

    override fun showRegisterCreditCard(contact: Contact) {
        activity.startActivity<CreditCardActivity>(CreditCardRouter.CONTACT_EXTRA to contact)
    }

    override fun showPayContactScreen(contact: Contact) {
        activity.startActivity<PaymentActivity>(PaymentRouter.CONTACT_EXTRA to contact)
    }

    companion object {

        fun assembleModule(view: ContactListActivity) {
            val remote = ContactsListRemoteDataManager()
            val local = ContactsListLocalDataManager()
            val repository = ContactRepository(local, remote)
            val interactor = ContactListInteractor(repository)
            val router = ContactListRouter(view)
            val presenter = ContactListPresenter(view, interactor, router, DisposeBag(view))
            view.presenter = presenter
        }
    }
}