# __VIPER__ Architecture with Kotlin

## Introduction

A simple application exemplifying some RESTFull methods using common techniques under RX and Kotlin.

This application shows how to use some of the most popular libraries under the *VIPER* (View, Interactor, Presenter, Entity, Router) architecture.

First of all, credits to the creators of these beauties libraries below that it were used in this project:
 - org.jetbrains.kotlin
 - com.squareup.okhttp3;
 - com.squareup.retrofit2;
 - io.reactivex.rxjava2;
 - com.orhanobut:hawk
 - com.fasterxml.jackson
 - com.squareup.picasso:picasso
 - com.trafi:anchor-bottom-sheet-behavior

 And Others.

It was developed with Android Studio 3.4 with `minSdkVersion 24` using native Kotlin for Android and it was migrated to AndroidX.

This project has a pipeline configured to build on every commit.

## Screenshots

![Lista Contatos](https://gitlab.com/aramizu/picme/raw/master/img01.png)
![Lista Contatos](https://gitlab.com/aramizu/picme/raw/master/img02.png)
![Lista Contatos](https://gitlab.com/aramizu/picme/raw/master/img03.png)
![Novo Cartão](https://gitlab.com/aramizu/picme/raw/master/img04.png)
![Cadastrar Cartão](https://gitlab.com/aramizu/picme/raw/master/img05.png)
![Cadastrar Cartão](https://gitlab.com/aramizu/picme/raw/master/img06.png)
![Pagar Contato](https://gitlab.com/aramizu/picme/raw/master/img07.png)
![Pagar Contato](https://gitlab.com/aramizu/picme/raw/master/img08.png)
![Error Transação](https://gitlab.com/aramizu/picme/raw/master/img09.png)
![Recibo](https://gitlab.com/aramizu/picme/raw/master/img10.png)
![Recibo](https://gitlab.com/aramizu/picme/raw/master/img11.png)

## Getting Started

Clone this repository, open this project with Android Studio 3.4 and just run.

## License

Just free to share. Best regards! :)